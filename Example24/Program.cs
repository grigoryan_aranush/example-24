﻿using System;
class Example
{
    static void Main()
    {
        int i;
        for (i = 0; i < 10; i++)
        {
            switch (i)
            {
                case 0:
                    Console.WriteLine("i is zero");
                    break;
                case 1:
                    Console.WriteLine("i is one");
                    break;
                case 2:
                    Console.WriteLine("i is two");
                    break;
                case 3:
                    Console.WriteLine("i is three");
                    break;
                default:
                    Console.WriteLine("i is more than three");
                    break;
            }
        }
    }
}